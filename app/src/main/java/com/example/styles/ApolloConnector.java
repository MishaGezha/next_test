package com.example.styles;

import android.util.Log;

import com.apollographql.apollo.ApolloClient;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.namespace.QName;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ApolloConnector {
    private static String TAG = "MaiActivity";
    private String sessionID;
    private static final String BASE_URL = "https://testing.csdev.com/_graphql";

    public static ApolloClient setupApollo(){
        CookieHandler cookieHandler = new CookieManager();

        final String saveUrl = "dfgdf";
        final HashMap<String, List<Cookie>> cookieStore = new HashMap<>();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cookieJar(
                        new CookieJar() {

                    @Override
                    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
                        cookieStore.put("saveUrl", cookies);
                        //saveUrl = url;
                        Log.d(TAG, "hkdshs");
                        for(Cookie list : cookies) {
                            Log.d(TAG, "cooka1" + String.valueOf(list));
                        }
                    }

                    @Override
                    public List<Cookie> loadForRequest(HttpUrl url) {
                       return  new  ArrayList<Cookie>();
                    }
                })
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        final Request original = chain.request();
                        //List<Cookie> cookies = cookieStore.get(saveUrl);
                        List<Cookie> cookies = cookieStore.get(saveUrl);
                        Log.d(TAG, "first");
                        if(cookies != null) {
                            Log.d(TAG, "first");
                            Iterator iterator = cookies.iterator();
                            while (iterator.hasNext()) {
                                Log.d(TAG, String.valueOf(iterator.next()));
                            }
                        }
                        Log.d(TAG, "second");
                        final Request authorized = original.newBuilder()
                                .addHeader("Cookie", "CSNSESSID=t8i1o8euhdhprgu4lmevvn3ggm")
                                .build();

                        return chain.proceed(authorized);
                    }
//                .cookieJar(new CookieJar() {
//                    @Override
//                    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
//                    }
//
//                    @Override
//                    public List<Cookie> loadForRequest(HttpUrl url) {
//                        final ArrayList<Cookie> oneCookie = new ArrayList<>(1);
//                        oneCookie.add({
//                        return new Cookie.Builder()
//                                .domain("publicobject.com")
//                                .path("/")
//                                .name("cookie-name")
//                                .value("cookie-value")
//                                .httpOnly()
//                                .secure()
//                                .build();
//        });
//                        return oneCookie;
//                    }
                })
                .build();
        return ApolloClient.builder().serverUrl(BASE_URL).okHttpClient(okHttpClient).build();
    }

}