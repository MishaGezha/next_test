package com.example.styles;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


public class Second_window extends AppCompatActivity {

    public TextView holla, code, texterror2;
    private final String TAG = "MaiActivity";
    private boolean i;
    private String id, ccode, errorString;
    private Button button2;
    private boolean transporter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_window);
        id = getIntent().getStringExtra("id");
        Log.d(TAG, "id: " + id);
        transporter = getIntent().getBooleanExtra("transporter", true);
        Log.d(TAG, "start2: ");
        start();
    }

    public void start() {
        code =(TextView)findViewById(R.id.code);
        holla = (TextView)findViewById(R.id.hello);
        texterror2 = (TextView)findViewById(R.id.texterror2);
        button2 = (Button)findViewById(R.id.button2);

        if(transporter) holla.setText("We sent you an email with a code. Please type it below:");
        else holla.setText("We sent");

        button2.setOnClickListener(
                view -> {
                    ccode = String.valueOf(code.getText());
                    ApolloConnector.setupApollo().mutate(
                            AuthStep2Mutation
                                    .builder()
                                    .code(ccode)
                                    .uuid(id)
                                    .build())
                            .enqueue(new ApolloCall.Callback<AuthStep2Mutation.Data>() {

                                @Override
                                public void onResponse(@NotNull Response<AuthStep2Mutation.Data> response) {
                                    //Log.d(TAG,"data: " + String.valueOf(response.data()));

                                    if(response.errors() != null && !response.errors().isEmpty()) {
                                        errorString = String.valueOf(response.errors());
                                    }
                                    else if(response.data().my.signIn.step.userErrors != null && !response.data().my.signIn.step.userErrors.isEmpty()) {
                                        errorString = String.valueOf(response.data().my.signIn.step.userErrors);
                                    }
                                    else {
                                        id = response.data().my.signIn.step.uuid;
                                    }
                                    i = true;
                                    Log.d(TAG,"progress..." + i);
                                }

                                @Override
                                public void onFailure(@NotNull ApolloException e) {
                                    Log.d(TAG, "Exception2 " +  e.getMessage());
                                    errorString = e.getMessage();
                                    i = true;
                                }
                            });
                    while (!i) {}
                    Log.d(TAG,"step 2");
                    if(errorString != null) {
                        Log.d(TAG, "it work, error");
                        texterror2.setText(errorString);
                        texterror2.setVisibility(View.VISIBLE);
                        code.setBackgroundResource(R.drawable.emailerror);
                        errorString = null;
                    }
                    else {
                        Log.d(TAG, "it work");
//                        Intent intent = new Intent("com.example.styles.Second_window");
//                        intent.putExtra("id", id);
//                        startActivity(intent);
                        texterror2.setVisibility(View.INVISIBLE);
                        code.setBackgroundResource(R.drawable.email);
                        onRestart();
                    }
                    i = false;
                }
        );
    }

    public void onBackPressed() {
        finish();
        Log.d(TAG, "finishing...");
    }
}
