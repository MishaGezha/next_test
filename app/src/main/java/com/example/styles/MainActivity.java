package com.example.styles;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;

import org.jetbrains.annotations.NotNull;


public class MainActivity extends AppCompatActivity {
    private final String TAG = "MaiActivity";
    private boolean i = false, otpTransport;
    private Button button;
    public String errorString = null, id, t;
    private TextView text, email, textError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG,"Start: ");
        listener();
    }

    public void listener() {
        button = (Button)findViewById(R.id.button);
        text = (TextView)findViewById(R.id.textView);
        textError = (TextView)findViewById(R.id.textView2);
        email = (TextView)findViewById(R.id.email);

        t = String.valueOf(email.getText());
        button.setOnClickListener(
                view -> {
                    ApolloConnector.setupApollo().mutate(
                            AuthStep1Mutation
                                    .builder()
                                    .phoneOrEmail(t)
                                    .build())
                            .enqueue(new ApolloCall.Callback<AuthStep1Mutation.Data>() {

                                @Override
                                public void onResponse(@NotNull Response<AuthStep1Mutation.Data> response) {

                                    if(response.errors() != null && !response.errors().isEmpty()) {
                                        errorString = String.valueOf(response.errors());
                                    }
                                    else if(response.data().my.signIn.step.userErrors != null && !response.data().my.signIn.step.userErrors.isEmpty()) {
                                        errorString = String.valueOf(response.data().my.signIn.step.userErrors);
                                    }
                                    else {
                                        id = response.data().my.signIn.step.uuid;
                                        if(response.data().my.signIn.step.otpTransport.equals("email")) otpTransport = true;
                                    }
                                    i = true;
                                    Log.d(TAG,"yes_" + i);
                                }

                                @Override
                                public void onFailure(@NotNull ApolloException e) {
                                    Log.d(TAG, "Exception1 " +  e.getMessage());
                                    errorString = e.getMessage();
                                    i = true;
                                    Log.d(TAG,"yes_");
                                }
                            });
                    Log.d(TAG,"_yes" + i);
                    while (!i) {}
                    Log.d(TAG,"step 2");
                    if(errorString != null) {
                        textError.setText(errorString);
                        textError.setVisibility(View.VISIBLE);
                        email.setBackgroundResource(R.drawable.emailerror);
                        errorString = null;
                    }
                    else {
                        Intent intent = new Intent("com.example.styles.Second_window");
                        intent.putExtra("id", id);
                        intent.putExtra("transporter", otpTransport);
                        startActivity(intent);
                        textError.setVisibility(View.INVISIBLE);
                        email.setBackgroundResource(R.drawable.email);
                        onRestart();
                    }
                    i = false;


                }
        );
    }
}